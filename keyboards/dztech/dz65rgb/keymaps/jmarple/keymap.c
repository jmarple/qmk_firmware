#include QMK_KEYBOARD_H

enum layers { BASE, RAISE, MOVE, RGB_LAYER};
enum custom_keycodes { LED_EN = SAFE_RANGE };

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
    [BASE] = LAYOUT_65_ansi(
        QK_GESC,        KC_1,    KC_2,    KC_3,    KC_4,    KC_5,    KC_6,    KC_7,    KC_8,    KC_9,    KC_0,    KC_MINS, KC_EQL,  KC_BSPC, KC_HOME,
        KC_TAB,         KC_Q,    KC_W,    KC_E,    KC_R,    KC_T,    KC_Y,    KC_U,    KC_I,    KC_O,    KC_P,    KC_LBRC, KC_RBRC, KC_BSLS, KC_PGUP,
        LT(MOVE, KC_ESC), LALT_T(KC_A),    LGUI_T(KC_S),   LSFT_T(KC_D),    LCTL_T(KC_F),    LT(RAISE, KC_G),    LT(RAISE, KC_H),   RCTL_T(KC_J),    RSFT_T(KC_K),    RGUI_T(KC_L),    RALT_T(KC_SCLN), KC_QUOT,          KC_ENT,  KC_PGDN,
        KC_LSFT,                 KC_Z,    KC_X,    KC_C,    KC_V,    KC_B,    KC_N,    KC_M,    KC_COMM, KC_DOT,  KC_SLSH, KC_RSFT, KC_UP,   KC_END,
        KC_LCTL,        KC_LGUI, KC_LALT,                            LT(MOVE, KC_SPC),                    KC_RALT, MO(RGB_LAYER),   KC_RCTL, KC_LEFT, KC_DOWN, KC_RGHT
    ),
    [RAISE] = LAYOUT_65_ansi(
        QK_GESC,        KC_F1,   KC_F2,   KC_F3,   KC_F4,   KC_F5,   KC_F6,   KC_F7,   KC_F8,   KC_F9,   KC_F10,  KC_F11,  KC_F12,  KC_DEL,  KC_HOME,
        _______,  KC_TILD, KC_EXLM, KC_AT,   KC_HASH, KC_DLR,  KC_PERC, KC_CIRC, KC_AMPR, KC_ASTR, KC_LPRN, KC_RPRN, KC_DEL, QK_BOOT, KC_PGUP,
        CTL_T(KC_CAPS), KC_DEL,  _______, _______, _______, _______, _______, LCTL_T(KC_MINS), LSFT_T(KC_EQL),  KC_LBRC, KC_RBRC, S(KC_EQL),KC_PIPE, KC_PGDN,
        KC_LSFT,                 _______, _______, _______, _______, _______, NK_TOGG, _______, _______, _______, _______, _______, KC_VOLU, KC_MUTE,
        _______,        _______, _______,                            _______,                   _______, _______, _______, KC_MPRV, KC_VOLD, KC_MNXT
    ),
    [MOVE] = LAYOUT_65_ansi(
        QK_GESC,        KC_F1,   KC_F2,   KC_F3,   KC_F4,   KC_F5,   KC_F6,   KC_F7,   KC_F8,   KC_F9,   KC_F10,  KC_F11,  KC_F12,  KC_DEL,  KC_HOME,
        _______,        RGB_TOG, RGB_MOD, RGB_HUI, RGB_HUD, RGB_SAI, RGB_SAD, RGB_VAI, RGB_VAD, _______, KC_PSCR, KC_SCRL, KC_PAUS, QK_BOOT, KC_PGUP,
        CTL_T(KC_CAPS), RGB_SPI, RGB_SPD, _______, _______, _______, LALT_T(KC_LEFT), LCTL_T(KC_DOWN), LSFT_T(KC_UP),   KC_RGHT,KC_QUOT, _______,          EE_CLR,  KC_PGDN,
        KC_LSFT, KC_MNXT, KC_VOLD, KC_VOLU, KC_MPLY, _______, KC_PIPE, KC_UNDS, KC_PLUS, KC_LCBR, KC_RCBR, KC_VOLU, KC_MUTE,
       _______, _______,        _______, _______,                            _______,                   _______, _______, _______, KC_MPRV, KC_VOLD, KC_MNXT
    ), 
    [RGB_LAYER] = LAYOUT_65_ansi(
        QK_GESC,        KC_F1,   KC_F2,   KC_F3,   KC_F4,   KC_F5,   KC_F6,   KC_F7,   KC_F8,   KC_F9,   KC_F10,  KC_F11,  KC_F12,  KC_DEL,  KC_HOME,
        _______,        RGB_TOG, RGB_MOD, RGB_HUI, RGB_HUD, RGB_SAI, RGB_SAD, RGB_VAI, RGB_VAD, _______, KC_PSCR, KC_SCRL, KC_PAUS, QK_BOOT, KC_PGUP,
        CTL_T(KC_CAPS), RGB_SPI, RGB_SPD, _______, _______, _______, _______, _______, _______, _______, _______, _______,          EE_CLR,  KC_PGDN,
        KC_LSFT,                 _______, _______, _______, _______, _______, NK_TOGG, _______, _______, _______, _______, _______, KC_VOLU, KC_MUTE,
        _______,        _______, _______,                            _______,                   _______, _______, _______, KC_MPRV, KC_VOLD, KC_MNXT

        ),
};

enum combos {
  X_C_TAB,
  C_V_BSPC,
  W_E_LSFT_LBRC,
  E_R_LSFT_RBRC,
  /* L_U_LGUI, */
  N_M_ENTER,
  M_COMMA_ESC,
  Q_W_GRAVE,
  I_O_SEMI,
};

const uint16_t PROGMEM x_c_tab[] = { KC_X, KC_C, COMBO_END};
const uint16_t PROGMEM c_v_bspc[] = { KC_C, KC_V, COMBO_END};
const uint16_t PROGMEM w_e_lsft_lbrc[] = { KC_W, KC_E, COMBO_END};
const uint16_t PROGMEM e_r_lsft_rbrc[] = { KC_E, KC_R, COMBO_END};
const uint16_t PROGMEM l_u_lgui	[] = { KC_L, KC_U, COMBO_END};
const uint16_t PROGMEM n_m_enter[] = { KC_N, KC_M, COMBO_END};
const uint16_t PROGMEM m_comma_esc[] = { KC_M, KC_COMM, COMBO_END};
const uint16_t PROGMEM q_w_grave[] = { KC_Q, KC_W, COMBO_END};
const uint16_t PROGMEM i_o_semi[] = { KC_I, KC_O, COMBO_END};

combo_t key_combos[COMBO_COUNT] = {
  [X_C_TAB] = COMBO(x_c_tab, KC_TAB),
  [C_V_BSPC] = COMBO(c_v_bspc, KC_BSPC),
  [W_E_LSFT_LBRC] = COMBO(w_e_lsft_lbrc, LSFT(KC_LBRC)),
  [E_R_LSFT_RBRC] = COMBO(e_r_lsft_rbrc, LSFT(KC_RBRC)),
  /* [L_U_LGUI	] = COMBO(l_u_lgui, KC_LGUI	), */
  [N_M_ENTER] = COMBO(n_m_enter, KC_ENT),
  [M_COMMA_ESC] = COMBO(m_comma_esc, KC_ESC),
  [Q_W_GRAVE] = COMBO(q_w_grave, KC_GRAVE),
  [I_O_SEMI] = COMBO(i_o_semi, KC_SCLN),
};
