// Copyright 2022 Takuya Urakawa @hsgw (dm9records.com, 5z6p.com)
// SPDX-License-Identifier: GPL-2.0-or-later

#include QMK_KEYBOARD_H

enum layers { BASE, RAISE, MOVE, CONF };
enum custom_keycodes { LED_EN = SAFE_RANGE };

// clang-format off
const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

    [BASE] = LAYOUT(
        KC_ESC,  KC_Q,    KC_W,    KC_E,    KC_R,    KC_T,    KC_LBRC,   KC_Y,   KC_U,    KC_I,    KC_O,    KC_P,     KC_BSPC,
        KC_TAB,  RGUI_T(KC_A),    LALT_T(KC_S),   LSFT_T(KC_D),    LCTL_T(KC_F),    KC_G,    KC_RBRC,   KC_H,   RCTL_T(KC_J),    RSFT_T(KC_K),    RALT_T(KC_L),    RGUI_T(KC_SCLN),  KC_QUOT,
        KC_LSFT, KC_Z,    KC_X,    KC_C,    KC_V,    KC_B,               KC_INS, KC_N,    KC_M,    KC_COMM, KC_DOT,   KC_SLSH,
        KC_LCTL,                   KC_LALT, KC_LGUI, LT(MOVE, KC_BSPC), MO(MOVE),    MO(RAISE), LT(RAISE, KC_SPC), KC_RALT, KC_MENU, KC_RCTL,  KC_RSFT
    ),
    [RAISE] = LAYOUT(
        KC_DEL,  KC_1,    KC_2,    KC_3,    KC_4,    KC_5,    _______,   KC_6,    KC_7,    KC_8,    KC_9,    KC_0,    KC_MINS, 
        KC_CAPS, KC_F1,   KC_F2,   LSFT_T(KC_F3),   LCTL_T(KC_F4),   LALT_T(KC_F5),   LALT_T(KC_F6),   _______, LCTL_T(KC_MINS), LSFT_T(KC_EQL),  KC_LBRC, KC_RBRC, S(KC_EQL),
        _______, KC_F7,   KC_F8,   KC_F9,   KC_F10,  KC_F11,             KC_F12,  KC_BSLS, _______, _______, _______,  KC_BSLS,
        _______,                   _______, _______, _______, _______,   _______, _______, _______, _______, _______,  _______
    ),
    [MOVE] = LAYOUT(
        KC_DEL,  KC_EXLM, KC_AT,   KC_HASH, KC_DLR,  KC_PERC,   KC_F6,     KC_CIRC, KC_AMPR, KC_ASTR, KC_LPRN, KC_RPRN,  KC_F12,
        _______, AG_NORM, AG_SWAP, LED_EN, KC_CAPS, _______, _______,   LALT_T(KC_LEFT), LCTL_T(KC_DOWN), LSFT_T(KC_UP),   KC_RGHT,KC_QUOT, _______,
        _______, KC_MNXT, KC_VOLD, KC_VOLU, KC_MPLY, _______,            KC_PIPE, KC_UNDS, KC_PLUS, KC_LCBR, KC_RCBR, _______,
        _______,                   _______, _______, _______, _______,   _______, _______, _______, _______, _______, _______
    ),
    [CONF] = LAYOUT(
        RESET,   XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,   XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,
        LED_EN,  XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,   XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,
        XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,            XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,
        XXXXXXX,                   XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,   XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX
    )
};

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
    switch (keycode) {
        case LED_EN:
            if (record->event.pressed) {
                lain_enable_leds_toggle();
            }
            return false;
        default:
            break;
    }
    return true;
}

// clang-format on

layer_state_t layer_state_set_user(layer_state_t state) {
    layer_state_t computed = update_tri_layer_state(state, RAISE, MOVE, CONF);
    switch (get_highest_layer(computed)) {
        case RAISE:
            lain_set_led(1, 1);
            lain_set_led(2, 0);
            break;
        case MOVE:
            lain_set_led(1, 0);
            lain_set_led(2, 1);
            break;
        case CONF:
            lain_set_led(1, 1);
            lain_set_led(2, 1);
            break;
        default:
            lain_set_led(1, 0);
            lain_set_led(2, 0);
            break;
    }
    return computed;
}

bool led_update_user(led_t led_state) {
    lain_set_led(0, led_state.caps_lock);
    return false;
}

enum combos {
  X_C_TAB,
  C_V_LALT_TAB,
  W_E_LSFT_LBRC,
  E_R_LSFT_RBRC,
  /* L_U_LGUI, */
  N_M_ENTER,
  M_COMMA_ESC,
  Q_W_GRAVE,
  I_O_SEMI,
};

const uint16_t PROGMEM x_c_tab[] = { KC_X, KC_C, COMBO_END};
const uint16_t PROGMEM c_v_lalt_tab[] = { KC_C, KC_V, COMBO_END};
const uint16_t PROGMEM w_e_lsft_lbrc[] = { KC_W, KC_E, COMBO_END};
const uint16_t PROGMEM e_r_lsft_rbrc[] = { KC_E, KC_R, COMBO_END};
const uint16_t PROGMEM l_u_lgui	[] = { KC_L, KC_U, COMBO_END};
const uint16_t PROGMEM n_m_enter[] = { KC_N, KC_M, COMBO_END};
const uint16_t PROGMEM m_comma_esc[] = { KC_M, KC_COMM, COMBO_END};
const uint16_t PROGMEM q_w_grave[] = { KC_Q, KC_W, COMBO_END};
const uint16_t PROGMEM i_o_semi[] = { KC_I, KC_O, COMBO_END};

combo_t key_combos[COMBO_COUNT] = {
  [X_C_TAB] = COMBO(x_c_tab, KC_TAB),
  [C_V_LALT_TAB] = COMBO(c_v_lalt_tab, LALT(KC_TAB)),
  [W_E_LSFT_LBRC] = COMBO(w_e_lsft_lbrc, LSFT(KC_LBRC)),
  [E_R_LSFT_RBRC] = COMBO(e_r_lsft_rbrc, LSFT(KC_RBRC)),
  /* [L_U_LGUI	] = COMBO(l_u_lgui, KC_LGUI	), */
  [N_M_ENTER] = COMBO(n_m_enter, KC_ENT),
  [M_COMMA_ESC] = COMBO(m_comma_esc, KC_ESC),
  [Q_W_GRAVE] = COMBO(q_w_grave, KC_GRAVE),
  [I_O_SEMI] = COMBO(i_o_semi, KC_SCLN),
};

uint16_t get_tapping_term(uint16_t keycode, keyrecord_t *record) {
    switch (keycode) {
        case SFT_T(KC_SPC):
            return TAPPING_TERM + 100;
        default:
            return TAPPING_TERM;
    }
}

bool get_ignore_mod_tap_interrupt(uint16_t keycode, keyrecord_t *record) {
    switch (keycode) {
      case LT(MOVE, KC_BSPC):
      case LT(RAISE, KC_SPC):
          return false;
      default:
          return true;
    }
}
