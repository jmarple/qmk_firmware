/* Good on you for modifying your layout! if you don't have
 * time to read the QMK docs, a list of keycodes can be found at
 *
 * https://github.com/qmk/qmk_firmware/blob/master/docs/keycodes.md
 *
 * There's also a template for adding new layers at the bottom of this file!
 */

#include QMK_KEYBOARD_H

/* #define IGNORE_MOD_TAP_INTERRUPT */
#define BASE 0 // default layer
#define SYMB 1 // symbols
#define NUMB 2 // numbers/motion

enum custom_keycodes {
  M1_STRING = SAFE_RANGE,
  M2_URL,
};

// Blank template at the bottom

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
/* Keymap 0: Basic layer
 *
 *       ,-------------------------------------------.                         ,-------------------------------------------.
 *       |  TAB   |   Q  |   W  |   F  |   P  |   G  |                         |   J  |   L  |   U  |   Y  | ; :  |  | \   |
 *       |--------+------+------+------+------+------|------.           .------|------+------+------+------+------+--------|
 *       |  Ctrl  |   A  |   R  |  S   |   T  |   D  |O(CMD)|           |O(CTL)|   H  |   N  |   E  |   I  |  O   |  ' "   |
 *       |--------+------+------+------+------+------|------|           |------|------+------+------+------+------+--------|
 *       | LShift |   Z  |   X  |   C  |   V  |   B  |O(ALT)|           |      |   K  |   M  | ,  < | . >  | /  ? | RShift |
 *       `--------+------+------+------+------+-------------'           `-------------+------+------+------+------+--------'
 *                          .----------.   .-------.                                 .------.   .--------.
 *                          | alt/del  |   | BKSP  |                                 | Space|   |cmd/del |
 *                          '----------'   '-------'                                 `------.   '--------'
 *                                              ,-------.                      ,-------.
 *                                              | MMB   |                      |  :    |
 *                                       ,------|-------|                      |-------|------.
 *                                       | NUMB | SYMB  |                      | SYMB  | NUMB |
 *                                       |  Esc |  F13  |                      | F14   | Enter|
 *                                       |      |       |                      |       |      |
 *                                       `--------------'                      `--------------'
 */
/* [BASE] = LAYOUT_gergo( */
/* KC_TAB,               KC_Q,  KC_W,   KC_F,   KC_P, KC_G,                                       KC_J,  KC_L,  KC_U,  KC_Y,KC_SCLN,  KC_BSLS, */
/* KC_LCTL,              KC_A,  KC_R,   LSFT_T(KC_S),    LCTL_T(KC_T),    LALT_T(KC_D), OSM(MOD_LGUI),       OSM(MOD_LCTL),   LALT_T(KC_H),    LCTL_T(KC_N),  LSFT_T(KC_E),  KC_I, KC_O, KC_QUOT, */
/* KC_LSFT,              KC_Z,  KC_X,   KC_C,   KC_V, KC_B, OSM(MOD_LALT), KC_TRNS, KC_K, KC_M,  KC_COMM, KC_DOT, KC_SLSH, KC_RSFT, */

/*                                      KC_ESC, LCTL_T(KC_BSPC),                           KC_SPC, CMD_T(KC_DEL), */

/*                                                          KC_BTN3,       KC_COLON, */
/*                                 MO(SYMB), LT(NUMB, KC_F13),     LT(NUMB, KC_F14), MO(NUMB)), */
[BASE] = LAYOUT_gergo(
KC_TAB,               KC_Q,  KC_W,   KC_F,   KC_P, KC_G,                                       KC_J,  KC_L,  KC_U,  KC_Y,KC_SCLN,  KC_BSLS,
KC_LCTL,              LGUI_T(KC_A),  LALT_T(KC_R),   LSFT_T(KC_S),    LCTL_T(KC_T),    KC_D, OSM(MOD_LGUI),       OSM(MOD_LCTL),   KC_H,    RCTL_T(KC_N),  RSFT_T(KC_E),  RALT_T(KC_I), RGUI_T(KC_O), KC_QUOT,
KC_LSFT,              KC_Z,  KC_X,   KC_C,   KC_V, KC_B, OSM(MOD_LALT), KC_BTN3, KC_COLON, KC_TRNS, KC_K, KC_M,  KC_COMM, KC_DOT, KC_SLSH, KC_RSFT,
                                     KC_ESC, LT(SYMB, KC_BSPC), MO(SYMB), LT(NUMB, KC_F13), LT(NUMB, KC_F14), MO(NUMB), LT(NUMB, KC_SPC), CMD_T(KC_DEL)),
/* Keymap 1: Symbols layer
 *
 * ,-------------------------------------------.                         ,-------------------------------------------.
 * |        |  !   |  @   |  #   |  $   |  %   |                         |   ^  |  &   |  *   |  (   |  )   |  VolUp |
 * |--------+------+------+------+------+------|------.           .------|------+------+------+------+------+--------|
 * |        |  [   |  ]   |  {   |  }   |  `   |  M1  |           |      |      |  -   |  _   |  +   |  =   |  VolDn |
 * |--------+------+------+------+------+------|------|           |------|------+------+------+------+------+--------|
 * |        |  `   |  ~   |      |      |  ~   |  M2  |           |      |      |      | Prev |Pl/Pau| Next |  Mute  |
 * `--------+------+------+------+------+-------------'           `-------------+------+------+------+------+--------'
 *                        .------.   .------.                                 .------.   .-----.
 *                        |      |   |      |                                 |      |   |     |
 *                        '------'   '------'                                 `------.   '-----'
 *                                        ,-------.                     ,-------.
 *                                        |       |                     |       |
 *                                 ,------|-------|                     |-------|------.
 *                                 |      |       |                     |       |      |
 *                                 |      |       |                     |       |      |
 *                                 |      |       |                     |       |      |
 *                                 `--------------'                     `--------------'
 */
[SYMB] = LAYOUT_gergo(
KC_TRNS, KC_EXLM, KC_AT,   KC_HASH,KC_DLR, KC_PERC,                            KC_CIRC,   KC_AMPR, KC_ASTR, KC_LPRN, KC_RPRN, KC__VOLUP,
KC_TRNS,KC_TRNS, LALT_T(KC_BSPC), LSFT_T(KC_TAB), LCTL_T(KC_ENT),   KC_PLUS,  M1_STRING, KC_TRNS,  KC_LEFT, RCTL_T(KC_DOWN),  RSFT_T(KC_UP), RALT_T(KC_RGHT), RGUI_T(KC_QUOT), KC__VOLDOWN,
KC_TRNS, KC_TRNS, KC_MNXT, KC_VOLD,        KC_VOLU, KC_MPLY, M2_URL, KC_TRNS,  KC_TRNS,   KC_TRNS,  KC_PIPE, KC_UNDS, KC_PLUS, KC_LCBR, KC_RCBR, KC__MUTE,

                                                   KC_TRNS,       KC_TRNS,

                                                           KC_TRNS,       KC_TRNS,
                                             	  KC_TRNS, KC_TRNS,        KC_TRNS, KC_TRNS),
/* Keymap 2: Pad/Function layer
 *
 * ,-------------------------------------------.                         ,-------------------------------------------.
 * |        |   1  |  2   |  3   |  4   |  5   |                         |  6   |  7   |  8   |  9   |  0   | PgUp   |
 * |--------+------+------+------+------+------|------.           .------|------+------+------+------+------+--------|
 * |  F1    |  F2  | F3   | F4   | F5   | F6   | BTN1 |           | Home | LEFT | DOWN |  UP  | RIGHT| End  | PgDn   |
 * |--------+------+------+------+------+------|------|           |------|------+------+------+------+------+--------|
 * |  F7    |  F8  | F9   | F10  | F11  | F12  | BTN2 |           |      | MLFT | MDWN | MUP  | MRGHT|      |        |
 * `--------+------+------+------+------+-------------'           `-------------+------+------+------+------+--------'
 *                        .------.   .------.                                 .------.   .-----.
 *                        |      |   |      |                                 |  ALT |   |     |
 *                        '------'   '------'                                 `------.   '-----'
 *                                        ,-------.                     ,-------.
 *                                        |       |                     |       |
 *                                 ,------|-------|                     |-------|------.
 *                                 |      |       |                     |       |      |
 *                                 |      |       |                     |       |      |
 *                                 |      |       |                     |       |      |
 *                                 `--------------'                     `--------------'
 */
[NUMB] = LAYOUT_gergo(
KC_TRNS, KC_1, 	  KC_2,    KC_3,    KC_4,    KC_5,                             KC_6,    KC_7,    KC_8,    KC_9,    KC_0,    KC_PGUP,
KC_TRNS, LGUI_T(KC_F1),   LALT_T(KC_F2),   LSFT_T(KC_F3),   LCTL_T(KC_F4),   KC_F5, KC_BTN1,         KC_HOME,  KC_F6,   LCTL_T(KC_MINS), LSFT_T(KC_EQL),  LALT_T(KC_LBRC), LGUI_T(KC_RBRC), KC_PGDN,
KC_TRNS, KC_F7,   KC_F8,   KC_F9,   KC_F10,  KC_F11, KC_BTN2,KC_TRNS, KC_TRNS,        KC_TRNS,  KC_F12,  KC_BSLS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,

                                                  LCTL_T(KC_DEL),       KC_RALT,

                                                           KC_TRNS,       KC_TRNS,
                                             	  KC_TRNS, KC_TRNS,       KC_TRNS, KC_TRNS)
};

/* Keymap template
 *
 * ,-------------------------------------------.                         ,-------------------------------------------.
 * |        |      |      |      |      |      |                         |      |      |      |      |      |        |
 * |--------+------+------+------+------+------|------.           .------|------+------+------+------+------+--------|
 * |        |      |      |      |      |      |      |           |      |      |      |      |      |      |        |
 * |--------+------+------+------+------+------|------|           |------|------+------+------+------+------+--------|
 * |        |      |      |      |      |      |      |           |      |      |      |      |      |      |        |
 * `--------+------+------+------+------+-------------'           `-------------+------+------+------+------+--------'
 *                        .------.   .------.                                 .------.   .-----.
 *                        |      |   |      |                                 |      |   |     |
 *                        '------'   '------'                                 `------.   '-----'
 *                                        ,-------.       ,-------.
 *                                        |       |       |       |
 *                                 ,------|-------|       |-------|------.
 *                                 |      |       |       |       |      |
 *                                 |      |       |       |       |      |
 *                                 |      |       |       |       |      |
 *                                 `--------------'       `--------------'
[SYMB] = LAYOUT_gergo(
KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,                          KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,       KC_TRNS,  KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,       KC_TRNS,  KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,  KC_TRNS, KC_TRNS,

                                                  KC_TRNS, KC_TRNS,       KC_TRNS, KC_TRNS,
                                                           KC_TRNS,       KC_TRNS,
                                             	  KC_TRNS, KC_TRNS,       KC_TRNS, KC_TRNS),
 */

// Runs just one time when the keyboard initializes.
void matrix_init_user(void) {

};

// Runs constantly in the background, in a loop.
void matrix_scan_user(void) {
    //uint8_t layer = biton32(layer_state);
    biton32(layer_state);
};


bool process_record_user(uint16_t keycode, keyrecord_t *record) {
  switch (keycode) {
    case M1_STRING:
      if (record->event.pressed) {
        // when keycode QMKBEST is pressed
        SEND_STRING("Hi!" SS_TAP(X_ENTER));
      } else {
        // when keycode QMKBEST is released
      }
      break;

    case M2_URL:
      if (record->event.pressed) {
          SEND_STRING("https://ddg.gg" SS_TAP(X_ENTER));
      }
      break;

  }
  return true;
};

enum combos {
  X_C_TAB,
  C_V_LALT_TAB,
  W_F_LSFT_LBRC,
  F_P_LSFT_RBRC,
  /* L_U_LGUI, */
  M_COMMA_ENTER,
  COMMA_PERIOD_ESC,
  Q_W_GRAVE,
  Y_U_COLON,
};

const uint16_t PROGMEM x_c_tab[] = { KC_X, KC_C, COMBO_END};
const uint16_t PROGMEM c_v_lalt_tab[] = { KC_C, KC_V, COMBO_END};
const uint16_t PROGMEM w_f_lsft_lbrc[] = { KC_W, KC_F, COMBO_END};
const uint16_t PROGMEM f_p_lsft_rbrc[] = { KC_F, KC_P, COMBO_END};
const uint16_t PROGMEM l_u_lgui	[] = { KC_L, KC_U, COMBO_END};
const uint16_t PROGMEM m_comma_enter[] = { KC_M, KC_COMM, COMBO_END};
const uint16_t PROGMEM comma_period_esc[] = { KC_COMM, KC_DOT, COMBO_END};
const uint16_t PROGMEM q_w_grave[] = { KC_Q, KC_W, COMBO_END};
const uint16_t PROGMEM y_u_colon[] = { KC_Y, KC_U, COMBO_END};

combo_t key_combos[COMBO_COUNT] = {
  [X_C_TAB] = COMBO(x_c_tab, KC_TAB),
  [C_V_LALT_TAB] = COMBO(c_v_lalt_tab, LALT(KC_TAB)),
  [W_F_LSFT_LBRC] = COMBO(w_f_lsft_lbrc, LSFT(KC_LBRC)),
  [F_P_LSFT_RBRC] = COMBO(f_p_lsft_rbrc, LSFT(KC_RBRC)),
  /* [L_U_LGUI	] = COMBO(l_u_lgui, KC_LGUI	), */
  [M_COMMA_ENTER] = COMBO(m_comma_enter, KC_ENT),
  [COMMA_PERIOD_ESC] = COMBO(comma_period_esc, KC_ESC),
  [Q_W_GRAVE] = COMBO(q_w_grave, KC_GRAVE),
  [Y_U_COLON] = COMBO(y_u_colon, KC_COLON),
};

/* uint16_t get_tapping_term(uint16_t keycode, keyrecord_t *record) { */
/*     switch (keycode) { */
/*         case SFT_T(KC_SPC): */
/*             return TAPPING_TERM + 100; */
/*         default: */
/*             return TAPPING_TERM; */
/*     } */
/* } */

/* bool get_ignore_mod_tap_interrupt(uint16_t keycode, keyrecord_t *record) { */
/*     switch (keycode) { */
/*       case LCTL_T(KC_BSPC): */
/*           return false; */
/*       default: */
/*           return true; */
/*     } */
/* } */
